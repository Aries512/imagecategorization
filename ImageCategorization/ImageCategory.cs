﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Structure;
using System.IO;

namespace ImageCategorization {
	
	/// <summary>
	/// Represents set of images of the same category.
	/// </summary>
	class ImageCategory {

		private List<Image> images;

		public List<Image> TrainingImages { get; private set; }
		public List<Image> TestingImages { get; private set; }

		public string Name { get; set; }



		/// <summary>
		/// Creates new category and fills it with all images stored in the
		/// specified directory.
		/// </summary>
		public ImageCategory(string directory, int trainingPercent = 100) {
			
			directory.ThrowIfNullOrEmpty();
			if (!Directory.Exists(directory)) {
				throw new ArgumentException("Directory does not exist!");
			}

			Image.SurfComputationType surfType = Image.SurfComputationType.CPU;
			Program.Stopwatches[Image.STOPWATCH_KEY].Reset();
			
			Name = Path.GetFileName(directory);
			images = new List<Image>();
			Logger.Add("Adding category " + Name + "...");
			
			foreach (string file in Directory.EnumerateFiles(directory)
				.Where(f => !endsWithOneOf(f.ToLower(), "surf", "txt", "xml"))) {
				try {
					Image.SurfComputationType usedType;
					images.Add(new Image(file, out usedType));
					if (usedType > surfType) {
						surfType = usedType;
					}
				} catch { }
			}

			Logger.Add("Loaded " + images.Count + " images."
				+ " Used " + surfType.ToString() + " for SURF features"
				+ (surfType == Image.SurfComputationType.CPU ? "." :  " where possible."));
			Program.Stopwatches.Log(Image.STOPWATCH_KEY);
			Logger.NewLine();

			trainingPercent = Math.Min(100, Math.Max(0, trainingPercent));
			TrainingImages = new List<Image>(images.Take(Math.Max(1, (int)(trainingPercent * images.Count / 100.0))));
			TestingImages = new List<Image>(images.Skip(TrainingImages.Count));
		}



		private bool endsWithOneOf(string str, params string[] endings) {
			return endings.Any(e => str.EndsWith(e));
		}



		public override string ToString() {
			return Name ?? "";
		}

	}
}
