﻿namespace ImageCategorization {
	partial class CategorizationSettingsControl {
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label2 = new System.Windows.Forms.Label();
			this.numericUpDownKmeansIter = new System.Windows.Forms.NumericUpDown();
			this.numericUpDownClusterCnt = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.numericUpDownSoftAssignM = new System.Windows.Forms.NumericUpDown();
			this.label4 = new System.Windows.Forms.Label();
			this.numericUpDownNeighbours = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.checkBoxSoftMargin = new System.Windows.Forms.CheckBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.numericUpDownSvmKfold = new System.Windows.Forms.NumericUpDown();
			this.label6 = new System.Windows.Forms.Label();
			this.numericUpDownSvmGamma = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.comboBoxSvmKernel = new System.Windows.Forms.ComboBox();
			this.comboBoxSvmType = new System.Windows.Forms.ComboBox();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownKmeansIter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownClusterCnt)).BeginInit();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownSoftAssignM)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownNeighbours)).BeginInit();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownSvmKfold)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownSvmGamma)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.numericUpDownKmeansIter);
			this.groupBox1.Controls.Add(this.numericUpDownClusterCnt);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(4, 4);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(200, 93);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Clustering";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(8, 52);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(93, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "K-means iterations";
			// 
			// numericUpDownKmeansIter
			// 
			this.numericUpDownKmeansIter.Location = new System.Drawing.Point(115, 45);
			this.numericUpDownKmeansIter.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numericUpDownKmeansIter.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numericUpDownKmeansIter.Name = "numericUpDownKmeansIter";
			this.numericUpDownKmeansIter.Size = new System.Drawing.Size(79, 20);
			this.numericUpDownKmeansIter.TabIndex = 2;
			this.numericUpDownKmeansIter.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
			// 
			// numericUpDownClusterCnt
			// 
			this.numericUpDownClusterCnt.Location = new System.Drawing.Point(115, 19);
			this.numericUpDownClusterCnt.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numericUpDownClusterCnt.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.numericUpDownClusterCnt.Name = "numericUpDownClusterCnt";
			this.numericUpDownClusterCnt.Size = new System.Drawing.Size(79, 20);
			this.numericUpDownClusterCnt.TabIndex = 1;
			this.numericUpDownClusterCnt.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 26);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(69, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Cluster count";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.numericUpDownSoftAssignM);
			this.groupBox2.Controls.Add(this.label4);
			this.groupBox2.Controls.Add(this.numericUpDownNeighbours);
			this.groupBox2.Controls.Add(this.label3);
			this.groupBox2.Controls.Add(this.checkBoxSoftMargin);
			this.groupBox2.Location = new System.Drawing.Point(210, 4);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(183, 93);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Neighbour search";
			// 
			// numericUpDownSoftAssignM
			// 
			this.numericUpDownSoftAssignM.DecimalPlaces = 2;
			this.numericUpDownSoftAssignM.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.numericUpDownSoftAssignM.Location = new System.Drawing.Point(95, 62);
			this.numericUpDownSoftAssignM.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
			this.numericUpDownSoftAssignM.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.numericUpDownSoftAssignM.Name = "numericUpDownSoftAssignM";
			this.numericUpDownSoftAssignM.Size = new System.Drawing.Size(79, 20);
			this.numericUpDownSoftAssignM.TabIndex = 4;
			this.numericUpDownSoftAssignM.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(10, 69);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(65, 13);
			this.label4.TabIndex = 3;
			this.label4.Text = "m-parameter";
			// 
			// numericUpDownNeighbours
			// 
			this.numericUpDownNeighbours.Location = new System.Drawing.Point(95, 36);
			this.numericUpDownNeighbours.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numericUpDownNeighbours.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numericUpDownNeighbours.Name = "numericUpDownNeighbours";
			this.numericUpDownNeighbours.Size = new System.Drawing.Size(79, 20);
			this.numericUpDownNeighbours.TabIndex = 5;
			this.numericUpDownNeighbours.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(10, 43);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(61, 13);
			this.label3.TabIndex = 4;
			this.label3.Text = "Neighbours";
			// 
			// checkBoxSoftMargin
			// 
			this.checkBoxSoftMargin.AutoSize = true;
			this.checkBoxSoftMargin.Location = new System.Drawing.Point(6, 19);
			this.checkBoxSoftMargin.Name = "checkBoxSoftMargin";
			this.checkBoxSoftMargin.Size = new System.Drawing.Size(99, 17);
			this.checkBoxSoftMargin.TabIndex = 0;
			this.checkBoxSoftMargin.Text = "Use soft margin";
			this.checkBoxSoftMargin.UseVisualStyleBackColor = true;
			this.checkBoxSoftMargin.CheckedChanged += new System.EventHandler(this.checkBoxSoftMargin_CheckedChanged);
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.label8);
			this.groupBox3.Controls.Add(this.label7);
			this.groupBox3.Controls.Add(this.numericUpDownSvmKfold);
			this.groupBox3.Controls.Add(this.label6);
			this.groupBox3.Controls.Add(this.numericUpDownSvmGamma);
			this.groupBox3.Controls.Add(this.label5);
			this.groupBox3.Controls.Add(this.comboBoxSvmKernel);
			this.groupBox3.Controls.Add(this.comboBoxSvmType);
			this.groupBox3.Location = new System.Drawing.Point(399, 4);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(190, 128);
			this.groupBox3.TabIndex = 2;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "SVM classification";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(6, 52);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(37, 13);
			this.label8.TabIndex = 11;
			this.label8.Text = "Kernel";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(6, 26);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(31, 13);
			this.label7.TabIndex = 10;
			this.label7.Text = "Type";
			// 
			// numericUpDownSvmKfold
			// 
			this.numericUpDownSvmKfold.Location = new System.Drawing.Point(99, 97);
			this.numericUpDownSvmKfold.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.numericUpDownSvmKfold.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numericUpDownSvmKfold.Name = "numericUpDownSvmKfold";
			this.numericUpDownSvmKfold.Size = new System.Drawing.Size(79, 20);
			this.numericUpDownSvmKfold.TabIndex = 9;
			this.numericUpDownSvmKfold.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(6, 104);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(34, 13);
			this.label6.TabIndex = 8;
			this.label6.Text = "K-fold";
			// 
			// numericUpDownSvmGamma
			// 
			this.numericUpDownSvmGamma.DecimalPlaces = 2;
			this.numericUpDownSvmGamma.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.numericUpDownSvmGamma.Location = new System.Drawing.Point(99, 71);
			this.numericUpDownSvmGamma.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
			this.numericUpDownSvmGamma.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.numericUpDownSvmGamma.Name = "numericUpDownSvmGamma";
			this.numericUpDownSvmGamma.Size = new System.Drawing.Size(79, 20);
			this.numericUpDownSvmGamma.TabIndex = 7;
			this.numericUpDownSvmGamma.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(6, 78);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(43, 13);
			this.label5.TabIndex = 6;
			this.label5.Text = "Gamma";
			// 
			// comboBoxSvmKernel
			// 
			this.comboBoxSvmKernel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxSvmKernel.FormattingEnabled = true;
			this.comboBoxSvmKernel.Location = new System.Drawing.Point(52, 46);
			this.comboBoxSvmKernel.Name = "comboBoxSvmKernel";
			this.comboBoxSvmKernel.Size = new System.Drawing.Size(126, 21);
			this.comboBoxSvmKernel.TabIndex = 1;
			this.comboBoxSvmKernel.SelectedIndexChanged += new System.EventHandler(this.comboBoxSvmKernel_SelectedIndexChanged);
			// 
			// comboBoxSvmType
			// 
			this.comboBoxSvmType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxSvmType.FormattingEnabled = true;
			this.comboBoxSvmType.Location = new System.Drawing.Point(52, 19);
			this.comboBoxSvmType.Name = "comboBoxSvmType";
			this.comboBoxSvmType.Size = new System.Drawing.Size(126, 21);
			this.comboBoxSvmType.TabIndex = 0;
			// 
			// CategorizationSettingsControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Name = "CategorizationSettingsControl";
			this.Size = new System.Drawing.Size(597, 135);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownKmeansIter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownClusterCnt)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownSoftAssignM)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownNeighbours)).EndInit();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownSvmKfold)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownSvmGamma)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown numericUpDownKmeansIter;
		private System.Windows.Forms.NumericUpDown numericUpDownClusterCnt;
		private System.Windows.Forms.NumericUpDown numericUpDownSoftAssignM;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.NumericUpDown numericUpDownNeighbours;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.CheckBox checkBoxSoftMargin;
		private System.Windows.Forms.NumericUpDown numericUpDownSvmGamma;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ComboBox comboBoxSvmKernel;
		private System.Windows.Forms.ComboBox comboBoxSvmType;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.NumericUpDown numericUpDownSvmKfold;
		private System.Windows.Forms.Label label6;
	}
}
