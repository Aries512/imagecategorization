﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ImageCategorization {
	partial class CategorizationSettingsControl : UserControl {
		
		public CategorizationSettingsControl() {
			InitializeComponent();
		}

		public void Populate(Categorizer.Params @params) {
			numericUpDownClusterCnt.Value = @params.ClusterCnt;
			numericUpDownKmeansIter.Value = @params.KmeansIterCnt;
			checkBoxSoftMargin.Checked = @params.UseSoftMargin;
			numericUpDownNeighbours.Value = @params.SoftMarginNeighbours;
			numericUpDownSoftAssignM.Value = (decimal)@params.SoftMarginM;
			comboBoxSvmType.Items.Clear();
			comboBoxSvmType.Items.AddRange(Enum.GetNames(typeof(Categorizer.Params.SVMType)));
			comboBoxSvmType.SelectedIndex = (int)@params.SvmType;
			comboBoxSvmKernel.Items.Clear();
			comboBoxSvmKernel.Items.AddRange(Enum.GetNames(typeof(Categorizer.Params.SVMKernelType)));
			comboBoxSvmKernel.SelectedIndex = (int)@params.SvmKernelType;
			numericUpDownSvmGamma.Value = (decimal)@params.SvmGamma;
			numericUpDownSvmKfold.Value = @params.SvmKfold;
		}

		public Categorizer.Params GetParams() {
			return new Categorizer.Params() {
				ClusterCnt = (int)numericUpDownClusterCnt.Value,
				KmeansIterCnt = (int)numericUpDownKmeansIter.Value,
				UseSoftMargin = checkBoxSoftMargin.Checked,
				SoftMarginNeighbours = (int)numericUpDownNeighbours.Value,
				SoftMarginM = (double)numericUpDownSoftAssignM.Value,
				SvmType = (Categorizer.Params.SVMType)Enum.Parse(typeof(Categorizer.Params.SVMType), comboBoxSvmType.SelectedItem.ToString(), true),
				SvmKernelType = (Categorizer.Params.SVMKernelType)Enum.Parse(typeof(Categorizer.Params.SVMKernelType), comboBoxSvmKernel.SelectedItem.ToString(), true),
				SvmGamma = (double)numericUpDownSvmGamma.Value,
				SvmKfold = (int)numericUpDownSvmKfold.Value
			};
		}

		private void checkBoxSoftMargin_CheckedChanged(object sender, EventArgs e) {
			numericUpDownNeighbours.Enabled = numericUpDownSoftAssignM.Enabled = checkBoxSoftMargin.Checked;
		}

		private void comboBoxSvmKernel_SelectedIndexChanged(object sender, EventArgs e) {
			numericUpDownSvmGamma.Enabled = comboBoxSvmKernel.SelectedItem.ToString() !=
				Categorizer.Params.SVMKernelType.Linear.ToString();
		}

		
	}
}
