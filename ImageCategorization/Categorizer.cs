﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.ML;

namespace ImageCategorization {
	
	/// <summary>
	/// Core of the application where all the magic is done.
	/// Handles training and classification.
	/// </summary>
	class Categorizer {


		/// <summary>
		/// Parameters for all the steps of the classifier training.
		/// </summary>
		public class @Params {

			public enum SVMType {
				Standard,
				OneToOne,
			}

			public enum SVMKernelType {
				Linear,
				RBF,
			}
			
			public int ClusterCnt = 25;
			public int KmeansIterCnt = 35;
			public bool UseSoftMargin = true;
			public int SoftMarginNeighbours = 3;
			public double SoftMarginM = 2;
			public SVMType SvmType = @Params.SVMType.OneToOne;
			public SVMKernelType SvmKernelType = SVMKernelType.RBF;
			public double SvmGamma = 5;
			public int SvmKfold = 25;
			
		}



		public const string TEST_NEIGHBOUR_STOPWATCH_KEY = "Testing (neighbour search)";
		public const string TEST_CLASSIFICATION_STOPWATCH_KEY = "Testing (classification)";
		public const string TRAIN_TOTAL_STOPWATCH_KEY = "Training (total)";
		public const string TRAIN_INIT_STOPWATCH_KEY = "Training (init)";
		public const string TRAIN_KMEANS_STOPWATCH_KEY = "Training (k-means)";
		public const string TRAIN_BAGS_STOPWATCH_KEY = "Training (bags initialization)";
		public const string TRAIN_SVM_STOPWATCH_KEY = "Training (SVM training)";

		private const int DESCRIPTOR_SIZE = 64;




		private @Params _params = new Params();
		public @Params @params {
			get {
				return _params;
			}
			set {
				if (_params != value) {
					//if paramaters change, current classifier becomes invalid
					_params = value;
					Trained = false;
				}
			}
		}
		
		private Matrix<float> clusterCenters;
		private FeatureTree kdTree;
		private ISvm svmModel;

		public List<string> CategoryNames { get; private set; }

		/// <summary>
		/// Whether categorizer is trained for the current data and parameters.
		/// Only trained categorizer can classify data.
		/// </summary>
		public bool Trained { get; set; }





		public Categorizer() {
			@params = new Params();
			Trained = false;
		}

		public Categorizer(Params @params) {
			this.@params = @params;
			Trained = false;
		}



		#region Train

		public void Train(List<ImageCategory> categories) {

			Program.Stopwatches[TRAIN_TOTAL_STOPWATCH_KEY].Restart();



			#region TrainInit

			Program.Stopwatches[TRAIN_INIT_STOPWATCH_KEY].Restart();

			//number of all images and descriptors
			int imgCnt = categories.Select(cat => cat.TrainingImages.Count).Sum();
			int descCnt = categories.Select(
				cat => cat.TrainingImages.Select(img => img.Descriptors.Rows).Sum())
				.Sum();

			Logger.Add("Starting training of " + categories.Count + " categories with "
				+ imgCnt + " images / " + descCnt + " descriptors...");

			CategoryNames = categories.Select(c => c.Name).ToList();

			//all descriptors
			Matrix<float> descriptors = new Matrix<float>(descCnt, DESCRIPTOR_SIZE);
			//index of category for each image
			Matrix<float> imgCategory = new Matrix<float>(imgCnt, 1);
			//index of image for each descriptor
			int[] descImg = new int[descCnt];

			int row = 0, descIdx = 0, imgIdx = 0, catIdx = 0, i, j;

			//fill descriptor list and associate descriptors with their images
			//and images with their categories
			foreach (ImageCategory cat in categories) {
				foreach (Image img in cat.TrainingImages) {
					var desc = img.Descriptors;
					int rowCnt = desc.Rows;
					for (i = 0; i < rowCnt; ++i, ++row) {
						for (j = 0; j < DESCRIPTOR_SIZE; ++j) descriptors[row, j] = desc[i, j];
						descImg[descIdx++] = imgIdx;
					}
					imgCategory[imgIdx++, 0] = catIdx;
				}
				catIdx++;
			}

			Program.Stopwatches.Log(TRAIN_INIT_STOPWATCH_KEY);

			#endregion TrainInit



			#region TrainClustering

			Program.Stopwatches[TRAIN_KMEANS_STOPWATCH_KEY].Restart();

			clusterCenters = new Matrix<float>(@params.ClusterCnt, DESCRIPTOR_SIZE);
			//corresponding center for each descriptor
			var labels = new Matrix<int>(descCnt, 1);
			CvInvoke.cvKMeans2(descriptors.Ptr, @params.ClusterCnt, labels,
				new MCvTermCriteria(@params.KmeansIterCnt), 2, IntPtr.Zero, 0, clusterCenters, IntPtr.Zero);

			Program.Stopwatches.Log(TRAIN_KMEANS_STOPWATCH_KEY);

			#endregion TrainClustering



			#region TrainBagsBuilding

			Program.Stopwatches[TRAIN_BAGS_STOPWATCH_KEY].Restart();

			kdTree = new FeatureTree(clusterCenters);

			var bags = new Matrix<float>(imgCnt, @params.ClusterCnt);
			if (!@params.UseSoftMargin) {
				for (i = 0; i < descCnt; ++i) {
					bags[descImg[i], labels[i, 0]]++;
				}
			} else {
				fillBagSoftAssign(descriptors, bags, descImg);
			}

			Program.Stopwatches.Log(TRAIN_BAGS_STOPWATCH_KEY);

			#endregion TrainBagsBuilding



			#region TrainSVM

			Program.Stopwatches[TRAIN_SVM_STOPWATCH_KEY].Restart();

			if (@params.SvmType == Params.SVMType.Standard) {
				svmModel = new SvmStandard();
			} else {
				svmModel = new SvmOneToOne();
			}
			svmModel.Train(bags, imgCategory, @params);

			Program.Stopwatches.Log(TRAIN_SVM_STOPWATCH_KEY);

			#endregion TrainSVM



			Program.Stopwatches.Log(TRAIN_TOTAL_STOPWATCH_KEY);
			Logger.Add("Training complete.");
			Logger.NewLine();

			Trained = true;

		}

		#endregion Train





		/// <param name="descImg">Index of image for each descriptor. <c>Null</c> if we have only one image
		/// (used when classifying uknown image).</param>
		private void fillBagSoftAssign(Matrix<float> descriptors, Matrix<float> bags, int[] descImg = null) {
			
			//distances of descriptors from their neighbours
			Matrix<double> nDist;
			int neighbourCnt = Math.Min(@params.SoftMarginNeighbours, @params.ClusterCnt);
			var neighbours = findClosestCentersKdtree(descriptors, neighbourCnt, out nDist);
			
			//weighted distance from each neighbour
			double[] p = new double[neighbourCnt];

			for (int i = 0; i < neighbours.Rows; ++i) {
				double sum = 0;
				for (int j = 0; j < neighbourCnt; ++j) {
					p[j] = Math.Exp(nDist[i, j] / @params.SoftMarginM);
					sum += p[j];
				}
				for (int j = 0; j < neighbourCnt; ++j) {
					int imgIdx = descImg == null ? 0 : descImg[i];
					int centerIdx = neighbours[i, j];
					bags[imgIdx, centerIdx] += (float)(p[j] / sum);
				}
			}
		}



		/// <returns>(Descriptor count x neighbour count) matrix storing indices of
		/// closest neighbours for each descriptor.</returns>
		private Matrix<int> findClosestCentersKdtree(Matrix<float> descriptors, int neighbourCnt,
			out Matrix<double> nDist) {
			Matrix<int> neighbours = new Matrix<int>(descriptors.Rows, neighbourCnt);
			nDist = new Matrix<double>(descriptors.Rows, neighbourCnt);
			//need to transform data for kdtree search.
			//no idea why they have used float[][] instead of Matrix<float>.
			float[][] tmp = new float[descriptors.Rows][];
			for (int i = 0; i < tmp.GetLength(0); ++i) {
				tmp[i] = new float[DESCRIPTOR_SIZE];
				for (int j = 0; j < DESCRIPTOR_SIZE; ++j) {
					tmp[i][j] = descriptors[i, j];
				}
			}
			kdTree.FindFeatures(tmp, out neighbours, out nDist, neighbourCnt, 20);
			return neighbours;
		}





		/// <summary>
		/// Finds category for the given image.
		/// Only trained categorizer can classify.
		/// </summary>
		/// <returns>Name of predicted category.</returns>
		public string CategorizeSample(Image testImg) {

			if (!Trained) {
				throw new InvalidOperationException("Must be trained before categorization!");
			}
			
			Matrix<float> bag = new Matrix<float>(1, @params.ClusterCnt);

			Program.Stopwatches[TEST_NEIGHBOUR_STOPWATCH_KEY].Start();

			if (!@params.UseSoftMargin) {
				Matrix<double> nDist;
				Matrix<int> neighbours = findClosestCentersKdtree(testImg.Descriptors, 1, out nDist);
				for (int i = 0; i < neighbours.Rows; ++i) {
					bag[0, neighbours[i, 0]]++;
				}
			} else {
				fillBagSoftAssign(testImg.Descriptors, bag);
			}
			
			Program.Stopwatches[TEST_NEIGHBOUR_STOPWATCH_KEY].Stop();

			Program.Stopwatches[TEST_CLASSIFICATION_STOPWATCH_KEY].Start();
			string result = CategoryNames[(int)svmModel.Classify(bag)];
			Program.Stopwatches[TEST_CLASSIFICATION_STOPWATCH_KEY].Stop();

			return result;
		}





		[System.Obsolete("Use kdTree search")]
		private int findClosestCenterBrute(Matrix<float> descriptors, int descRowIdx) {
			int closest = 0;
			float minDist = float.MaxValue;
			for (int j = 0; j < clusterCenters.Rows; ++j) {
				float dist = distSq(descriptors, descRowIdx, clusterCenters, j);
				if (dist < minDist) {
					minDist = dist;
					closest = j;
				}
			}
			return closest;
		}

		private float distSq(Matrix<float> a, int rowA, Matrix<float> b, int rowB) {
			float res = 0;
			for (int i = 0; i < a.Cols; ++i) res += (a[rowA, i] - b[rowB, i]) * (a[rowA, i] - b[rowB, i]);
			return res;
		}


	}
}
