﻿namespace ImageCategorization {
	partial class Form1 {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.buttonTrain = new System.Windows.Forms.Button();
			this.buttonTestSelectedImage = new System.Windows.Forms.Button();
			this.buttonTestSelectedCategory = new System.Windows.Forms.Button();
			this.buttonTestAll = new System.Windows.Forms.Button();
			this.textBoxLog = new System.Windows.Forms.TextBox();
			this.groupBoxTrain = new System.Windows.Forms.GroupBox();
			this.categorizationSettingsControl1 = new ImageCategorization.CategorizationSettingsControl();
			this.labelTrainingImgs = new System.Windows.Forms.Label();
			this.labelTestingImgs = new System.Windows.Forms.Label();
			this.buttonTestUnknownImage = new System.Windows.Forms.Button();
			this.labelClickHint = new System.Windows.Forms.Label();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.deleteCachedSURFsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.groupBoxCategories = new System.Windows.Forms.GroupBox();
			this.buttonAddCategories = new System.Windows.Forms.Button();
			this.labelCategories = new System.Windows.Forms.Label();
			this.buttonRemoveCategory = new System.Windows.Forms.Button();
			this.numericUpDownTrainingSetSize = new System.Windows.Forms.NumericUpDown();
			this.labelTrainingPercent = new System.Windows.Forms.Label();
			this.buttonAddCategory = new System.Windows.Forms.Button();
			this.comboBoxCategories = new System.Windows.Forms.ComboBox();
			this.groupBoxTesting = new System.Windows.Forms.GroupBox();
			this.listBoxImagesTrain = new ImageCategorization.ClickableListBox();
			this.listBoxImagesTest = new ImageCategorization.ClickableListBox();
			this.labelAuthor = new System.Windows.Forms.Label();
			this.groupBoxTrain.SuspendLayout();
			this.menuStrip1.SuspendLayout();
			this.groupBoxCategories.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownTrainingSetSize)).BeginInit();
			this.groupBoxTesting.SuspendLayout();
			this.SuspendLayout();
			// 
			// buttonTrain
			// 
			this.buttonTrain.Location = new System.Drawing.Point(10, 128);
			this.buttonTrain.Name = "buttonTrain";
			this.buttonTrain.Size = new System.Drawing.Size(75, 23);
			this.buttonTrain.TabIndex = 3;
			this.buttonTrain.Text = "Train";
			this.buttonTrain.UseVisualStyleBackColor = true;
			this.buttonTrain.Click += new System.EventHandler(this.buttonTrain_Click);
			// 
			// buttonTestSelectedImage
			// 
			this.buttonTestSelectedImage.Location = new System.Drawing.Point(470, 68);
			this.buttonTestSelectedImage.Name = "buttonTestSelectedImage";
			this.buttonTestSelectedImage.Size = new System.Drawing.Size(131, 23);
			this.buttonTestSelectedImage.TabIndex = 4;
			this.buttonTestSelectedImage.Text = "Test selected image";
			this.buttonTestSelectedImage.UseVisualStyleBackColor = true;
			this.buttonTestSelectedImage.Click += new System.EventHandler(this.buttonTestSelectedImage_Click);
			// 
			// buttonTestSelectedCategory
			// 
			this.buttonTestSelectedCategory.Location = new System.Drawing.Point(470, 97);
			this.buttonTestSelectedCategory.Name = "buttonTestSelectedCategory";
			this.buttonTestSelectedCategory.Size = new System.Drawing.Size(131, 23);
			this.buttonTestSelectedCategory.TabIndex = 8;
			this.buttonTestSelectedCategory.Text = "Test selected category";
			this.buttonTestSelectedCategory.UseVisualStyleBackColor = true;
			this.buttonTestSelectedCategory.Click += new System.EventHandler(this.buttonTestSelectedCategory_Click);
			// 
			// buttonTestAll
			// 
			this.buttonTestAll.Location = new System.Drawing.Point(470, 126);
			this.buttonTestAll.Name = "buttonTestAll";
			this.buttonTestAll.Size = new System.Drawing.Size(131, 23);
			this.buttonTestAll.TabIndex = 9;
			this.buttonTestAll.Text = "Test all categories";
			this.buttonTestAll.UseVisualStyleBackColor = true;
			this.buttonTestAll.Click += new System.EventHandler(this.buttonTestAll_Click);
			// 
			// textBoxLog
			// 
			this.textBoxLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxLog.Location = new System.Drawing.Point(12, 495);
			this.textBoxLog.Multiline = true;
			this.textBoxLog.Name = "textBoxLog";
			this.textBoxLog.ReadOnly = true;
			this.textBoxLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.textBoxLog.Size = new System.Drawing.Size(607, 145);
			this.textBoxLog.TabIndex = 11;
			// 
			// groupBoxTrain
			// 
			this.groupBoxTrain.Controls.Add(this.buttonTrain);
			this.groupBoxTrain.Controls.Add(this.categorizationSettingsControl1);
			this.groupBoxTrain.Location = new System.Drawing.Point(12, 122);
			this.groupBoxTrain.Name = "groupBoxTrain";
			this.groupBoxTrain.Size = new System.Drawing.Size(607, 157);
			this.groupBoxTrain.TabIndex = 15;
			this.groupBoxTrain.TabStop = false;
			this.groupBoxTrain.Text = "Training";
			// 
			// categorizationSettingsControl1
			// 
			this.categorizationSettingsControl1.Location = new System.Drawing.Point(6, 19);
			this.categorizationSettingsControl1.Name = "categorizationSettingsControl1";
			this.categorizationSettingsControl1.Size = new System.Drawing.Size(597, 135);
			this.categorizationSettingsControl1.TabIndex = 13;
			// 
			// labelTrainingImgs
			// 
			this.labelTrainingImgs.AutoSize = true;
			this.labelTrainingImgs.Location = new System.Drawing.Point(3, 23);
			this.labelTrainingImgs.Name = "labelTrainingImgs";
			this.labelTrainingImgs.Size = new System.Drawing.Size(81, 13);
			this.labelTrainingImgs.TabIndex = 16;
			this.labelTrainingImgs.Text = "Training images";
			// 
			// labelTestingImgs
			// 
			this.labelTestingImgs.AutoSize = true;
			this.labelTestingImgs.Location = new System.Drawing.Point(236, 25);
			this.labelTestingImgs.Name = "labelTestingImgs";
			this.labelTestingImgs.Size = new System.Drawing.Size(78, 13);
			this.labelTestingImgs.TabIndex = 17;
			this.labelTestingImgs.Text = "Testing images";
			// 
			// buttonTestUnknownImage
			// 
			this.buttonTestUnknownImage.Location = new System.Drawing.Point(470, 39);
			this.buttonTestUnknownImage.Name = "buttonTestUnknownImage";
			this.buttonTestUnknownImage.Size = new System.Drawing.Size(131, 23);
			this.buttonTestUnknownImage.TabIndex = 18;
			this.buttonTestUnknownImage.Text = "Test unknown image";
			this.buttonTestUnknownImage.UseVisualStyleBackColor = true;
			this.buttonTestUnknownImage.Click += new System.EventHandler(this.buttonTestUnknownImage_Click);
			// 
			// labelClickHint
			// 
			this.labelClickHint.AutoSize = true;
			this.labelClickHint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.labelClickHint.Location = new System.Drawing.Point(7, 154);
			this.labelClickHint.Name = "labelClickHint";
			this.labelClickHint.Size = new System.Drawing.Size(210, 13);
			this.labelClickHint.TabIndex = 20;
			this.labelClickHint.Text = "Double click on items in lists to show image";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteCachedSURFsToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// deleteCachedSURFsToolStripMenuItem
			// 
			this.deleteCachedSURFsToolStripMenuItem.Name = "deleteCachedSURFsToolStripMenuItem";
			this.deleteCachedSURFsToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
			this.deleteCachedSURFsToolStripMenuItem.Text = "Delete cached SURFs";
			this.deleteCachedSURFsToolStripMenuItem.Click += new System.EventHandler(this.deleteCachedSURFsToolStripMenuItem_Click);
			// 
			// aboutToolStripMenuItem
			// 
			this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem});
			this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
			this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
			this.aboutToolStripMenuItem.Text = "About";
			// 
			// helpToolStripMenuItem
			// 
			this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
			this.helpToolStripMenuItem.Size = new System.Drawing.Size(99, 22);
			this.helpToolStripMenuItem.Text = "Help";
			this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
			// 
			// menuStrip1
			// 
			this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.aboutToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(631, 24);
			this.menuStrip1.TabIndex = 19;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// groupBoxCategories
			// 
			this.groupBoxCategories.Controls.Add(this.buttonAddCategories);
			this.groupBoxCategories.Controls.Add(this.labelCategories);
			this.groupBoxCategories.Controls.Add(this.buttonRemoveCategory);
			this.groupBoxCategories.Controls.Add(this.numericUpDownTrainingSetSize);
			this.groupBoxCategories.Controls.Add(this.labelTrainingPercent);
			this.groupBoxCategories.Controls.Add(this.buttonAddCategory);
			this.groupBoxCategories.Controls.Add(this.comboBoxCategories);
			this.groupBoxCategories.Location = new System.Drawing.Point(12, 27);
			this.groupBoxCategories.Name = "groupBoxCategories";
			this.groupBoxCategories.Size = new System.Drawing.Size(607, 78);
			this.groupBoxCategories.TabIndex = 22;
			this.groupBoxCategories.TabStop = false;
			this.groupBoxCategories.Text = "Categories";
			// 
			// buttonAddCategories
			// 
			this.buttonAddCategories.Location = new System.Drawing.Point(327, 17);
			this.buttonAddCategories.Name = "buttonAddCategories";
			this.buttonAddCategories.Size = new System.Drawing.Size(92, 23);
			this.buttonAddCategories.TabIndex = 28;
			this.buttonAddCategories.Text = "Add Categories";
			this.buttonAddCategories.UseVisualStyleBackColor = true;
			this.buttonAddCategories.Click += new System.EventHandler(this.buttonAddCategories_Click);
			// 
			// labelCategories
			// 
			this.labelCategories.AutoSize = true;
			this.labelCategories.Location = new System.Drawing.Point(-194, -6);
			this.labelCategories.Name = "labelCategories";
			this.labelCategories.Size = new System.Drawing.Size(57, 13);
			this.labelCategories.TabIndex = 27;
			this.labelCategories.Text = "Categories";
			// 
			// buttonRemoveCategory
			// 
			this.buttonRemoveCategory.Location = new System.Drawing.Point(220, 46);
			this.buttonRemoveCategory.Name = "buttonRemoveCategory";
			this.buttonRemoveCategory.Size = new System.Drawing.Size(101, 23);
			this.buttonRemoveCategory.TabIndex = 26;
			this.buttonRemoveCategory.Text = "Remove Category";
			this.buttonRemoveCategory.UseVisualStyleBackColor = true;
			this.buttonRemoveCategory.Click += new System.EventHandler(this.buttonRemoveCategory_Click);
			// 
			// numericUpDownTrainingSetSize
			// 
			this.numericUpDownTrainingSetSize.Location = new System.Drawing.Point(548, 19);
			this.numericUpDownTrainingSetSize.Name = "numericUpDownTrainingSetSize";
			this.numericUpDownTrainingSetSize.Size = new System.Drawing.Size(53, 20);
			this.numericUpDownTrainingSetSize.TabIndex = 25;
			this.numericUpDownTrainingSetSize.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
			// 
			// labelTrainingPercent
			// 
			this.labelTrainingPercent.AutoSize = true;
			this.labelTrainingPercent.Location = new System.Drawing.Point(442, 27);
			this.labelTrainingPercent.Name = "labelTrainingPercent";
			this.labelTrainingPercent.Size = new System.Drawing.Size(100, 13);
			this.labelTrainingPercent.TabIndex = 24;
			this.labelTrainingPercent.Text = "Training set size [%]";
			// 
			// buttonAddCategory
			// 
			this.buttonAddCategory.Location = new System.Drawing.Point(220, 17);
			this.buttonAddCategory.Name = "buttonAddCategory";
			this.buttonAddCategory.Size = new System.Drawing.Size(101, 23);
			this.buttonAddCategory.TabIndex = 23;
			this.buttonAddCategory.Text = "Add Category";
			this.buttonAddCategory.UseVisualStyleBackColor = true;
			this.buttonAddCategory.Click += new System.EventHandler(this.buttonAddCategory_Click);
			// 
			// comboBoxCategories
			// 
			this.comboBoxCategories.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxCategories.FormattingEnabled = true;
			this.comboBoxCategories.Location = new System.Drawing.Point(6, 18);
			this.comboBoxCategories.Name = "comboBoxCategories";
			this.comboBoxCategories.Size = new System.Drawing.Size(208, 21);
			this.comboBoxCategories.TabIndex = 22;
			this.comboBoxCategories.SelectedIndexChanged += new System.EventHandler(this.comboBoxCategories_SelectedIndexChanged);
			// 
			// groupBoxTesting
			// 
			this.groupBoxTesting.Controls.Add(this.listBoxImagesTrain);
			this.groupBoxTesting.Controls.Add(this.buttonTestSelectedImage);
			this.groupBoxTesting.Controls.Add(this.labelClickHint);
			this.groupBoxTesting.Controls.Add(this.listBoxImagesTest);
			this.groupBoxTesting.Controls.Add(this.buttonTestUnknownImage);
			this.groupBoxTesting.Controls.Add(this.buttonTestSelectedCategory);
			this.groupBoxTesting.Controls.Add(this.labelTestingImgs);
			this.groupBoxTesting.Controls.Add(this.buttonTestAll);
			this.groupBoxTesting.Controls.Add(this.labelTrainingImgs);
			this.groupBoxTesting.Location = new System.Drawing.Point(12, 298);
			this.groupBoxTesting.Name = "groupBoxTesting";
			this.groupBoxTesting.Size = new System.Drawing.Size(607, 179);
			this.groupBoxTesting.TabIndex = 23;
			this.groupBoxTesting.TabStop = false;
			this.groupBoxTesting.Text = "Testing";
			// 
			// listBoxImagesTrain
			// 
			this.listBoxImagesTrain.FormattingEnabled = true;
			this.listBoxImagesTrain.Location = new System.Drawing.Point(6, 39);
			this.listBoxImagesTrain.Name = "listBoxImagesTrain";
			this.listBoxImagesTrain.Size = new System.Drawing.Size(200, 108);
			this.listBoxImagesTrain.TabIndex = 2;
			// 
			// listBoxImagesTest
			// 
			this.listBoxImagesTest.FormattingEnabled = true;
			this.listBoxImagesTest.Location = new System.Drawing.Point(239, 39);
			this.listBoxImagesTest.Name = "listBoxImagesTest";
			this.listBoxImagesTest.Size = new System.Drawing.Size(200, 108);
			this.listBoxImagesTest.TabIndex = 7;
			this.listBoxImagesTest.SelectedIndexChanged += new System.EventHandler(this.listBoxImagesTest_SelectedIndexChanged);
			// 
			// labelAuthor
			// 
			this.labelAuthor.AutoSize = true;
			this.labelAuthor.Location = new System.Drawing.Point(530, 9);
			this.labelAuthor.Name = "labelAuthor";
			this.labelAuthor.Size = new System.Drawing.Size(89, 13);
			this.labelAuthor.TabIndex = 24;
			this.labelAuthor.Text = "Matej Kopernický";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(631, 652);
			this.Controls.Add(this.labelAuthor);
			this.Controls.Add(this.groupBoxTesting);
			this.Controls.Add(this.groupBoxCategories);
			this.Controls.Add(this.groupBoxTrain);
			this.Controls.Add(this.textBoxLog);
			this.Controls.Add(this.menuStrip1);
			this.MaximumSize = new System.Drawing.Size(647, 100000);
			this.MinimumSize = new System.Drawing.Size(647, 690);
			this.Name = "Form1";
			this.Text = "Image Categorization";
			this.groupBoxTrain.ResumeLayout(false);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.groupBoxCategories.ResumeLayout(false);
			this.groupBoxCategories.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownTrainingSetSize)).EndInit();
			this.groupBoxTesting.ResumeLayout(false);
			this.groupBoxTesting.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private ClickableListBox listBoxImagesTrain;
		private System.Windows.Forms.Button buttonTrain;
		private System.Windows.Forms.Button buttonTestSelectedImage;
		private ClickableListBox listBoxImagesTest;
		private System.Windows.Forms.Button buttonTestSelectedCategory;
		private System.Windows.Forms.Button buttonTestAll;
		private System.Windows.Forms.TextBox textBoxLog;
		private CategorizationSettingsControl categorizationSettingsControl1;
		private System.Windows.Forms.GroupBox groupBoxTrain;
		private System.Windows.Forms.Label labelTrainingImgs;
		private System.Windows.Forms.Label labelTestingImgs;
		private System.Windows.Forms.Button buttonTestUnknownImage;
		private System.Windows.Forms.Label labelClickHint;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem deleteCachedSURFsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.GroupBox groupBoxCategories;
		private System.Windows.Forms.Button buttonAddCategories;
		private System.Windows.Forms.Label labelCategories;
		private System.Windows.Forms.Button buttonRemoveCategory;
		private System.Windows.Forms.NumericUpDown numericUpDownTrainingSetSize;
		private System.Windows.Forms.Label labelTrainingPercent;
		private System.Windows.Forms.Button buttonAddCategory;
		private System.Windows.Forms.ComboBox comboBoxCategories;
		private System.Windows.Forms.GroupBox groupBoxTesting;
		private System.Windows.Forms.Label labelAuthor;
	}
}

