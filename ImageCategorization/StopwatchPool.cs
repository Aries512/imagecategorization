﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace ImageCategorization {
	class StopwatchPool {


		private Dictionary<string, Stopwatch> pool = new Dictionary<string, Stopwatch>();



		/// <summary>
		/// Returns existing stopwatch or returns new, if it is accessed for the first time.
		/// </summary>
		public Stopwatch this[string name] {
			get {
				if (pool.ContainsKey(name)) {
					return pool[name];
				} else {
					Stopwatch sw = new Stopwatch();
					pool.Add(name, sw);
					return sw;
				}	
			}
		}



		public void StopAndResetAll() {
			pool.Values.ForEach(sw => sw.Reset());
		}



		public void Log(string name, string additional = null) {
			if (pool.ContainsKey(name)) {
				Logger.Add(name + (additional.IsNullOrEmpty() ? "" : ("(" + additional + ")"))
					+ ": " + pool[name].ElapsedMilliseconds + "ms");
			}
		}

		public void LogAndRestart(string name, string additional = null) {
			if (pool.ContainsKey(name)) {
				Log(name, additional);
				pool[name].Restart();
			}
		}

	}
}
