﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ImageCategorization {
	static class Logger {

		public static List<TextBox> TextBoxes { get; private set; }

		static Logger() {
			TextBoxes = new List<TextBox>();
		}

		public static void NewLine() {
			Add(null);
		}

		public static void Add(string str) {
			if (str == null) {
				str = "";
			}
			Console.WriteLine(str);
			TextBoxes.ForEach(tb => tb.AppendText(str + Environment.NewLine));
		}

	}
}
