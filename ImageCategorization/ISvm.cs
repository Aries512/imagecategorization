﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;

namespace ImageCategorization {
	interface ISvm {

		bool Trained { get; }
		
		void Train(Matrix<float> data, Matrix<float> categories, Categorizer.Params @params);

		float Classify(Matrix<float> datum);


	}
}
