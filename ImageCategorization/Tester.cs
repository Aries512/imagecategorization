﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImageCategorization {
	static class Tester {

		public static bool TestSample(Image img, Categorizer categorizer, out string result, string expected = null) {
			result = categorizer.CategorizeSample(img);
			return expected.IsNullOrEmpty() ? true : result.ToLower() == expected.ToLower();
		}



		public static int TestCategory(ImageCategory category, Categorizer categorizer,
			out List<string> results, out double success) {
			int correct = 0;
			results = category.TestingImages.Select(img => {
				string res;
				if (TestSample(img, categorizer, out res, category.Name)) {
					correct++;
				}
				return res;
			}).ToList();
			success = (double)correct / category.TestingImages.Count;
			return correct;
		}



		public static int TestCategories(List<ImageCategory> categories, Categorizer categorizer,
			out List<List<string>> results, out double totalSuccess, out double avgSuccess) {
			int correctTotal = 0;
			int total = 0;
			double successTotal = 0;
			var resultsTmp = new List<List<string>>();
			categories.ForEach(cat => {
				total += cat.TestingImages.Count;
				double success;
				List<string> result;
				correctTotal += TestCategory(cat, categorizer, out result, out success);
				successTotal += success;
				resultsTmp.Add(result);
			});
			results = resultsTmp;
			avgSuccess = successTotal / categories.Count;
			totalSuccess = (double)correctTotal / total;
			return correctTotal;
		}


	}
}
