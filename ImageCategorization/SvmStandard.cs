﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV.ML;
using Emgu.CV.Structure;
using Emgu.CV;

namespace ImageCategorization {
	
	/// <summary>
	/// Implements standard OpenCV's SVM and simplifies its functionality.
	/// </summary>
	class SvmStandard : ISvm {

		private bool _trained = false;
		public bool Trained {
			get {
				return _trained;
			}
		}

		private SVM model = null;



		public void Train(Matrix<float> data, Matrix<float> categories, Categorizer.Params @params) {
			
			if (data.Rows != categories.Rows) {
				throw new ArgumentException("Data and its categories must have same number of rows!");
			}
			
			model = new SVM();
			SVMParams p = new SVMParams() {
				KernelType = @params.SvmKernelType == Categorizer.Params.SVMKernelType.Linear ?
					Emgu.CV.ML.MlEnum.SVM_KERNEL_TYPE.LINEAR : Emgu.CV.ML.MlEnum.SVM_KERNEL_TYPE.RBF,
				SVMType = Emgu.CV.ML.MlEnum.SVM_TYPE.C_SVC,
				C = 1,
				Gamma = @params.SvmGamma,
				TermCrit = new MCvTermCriteria(150, 0.00001),
			};
			model.TrainAuto(data, categories, null, null, p.MCvSVMParams, @params.SvmKfold);
			
			_trained = true;
		}



		public float Classify(Matrix<float> datum) {
			if (!_trained) {
				throw new InvalidOperationException("Must be trained first!");
			}
			return model.Predict(datum);
		}
	}
}
