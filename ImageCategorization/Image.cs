﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
//using Emgu.CV.GPU;
using Emgu.CV.Util;
using Emgu.CV.Structure;
using Emgu.CV.Features2D;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Diagnostics;

using EmguImg = Emgu.CV.Image<Emgu.CV.Structure.Bgr, System.Byte>;
//using EmguImgGpu = Emgu.CV.GPU.GpuImage<Emgu.CV.Structure.Bgr, System.Byte>;

//GPU computation disabled for now because of older version of EMGU CV

namespace ImageCategorization {
	
	/// <summary>
	/// Represents image and its pre-calculated descriptors.
	/// Doesn't store loaded image itself, only path to it.
	/// </summary>
	class Image {



		public enum SurfComputationType {
			CPU,
			GPU,
			Cache
		}

		public const string STOPWATCH_KEY = "Images loading";



		public string Name { get; set; }

		private string imgPath;

		//SURF descriptors
		public Matrix<float> Descriptors { get; private set; }

		//For SURF detection
		private const int HESSIAN_THRESH = 500;

		//GPU SURF features calculation doesn't work for small images.
		//These are empirically found constants that should be big enough.
		private const int MIN_GPU_IMG_WIDTH = 200;
		private const int MIN_GPU_IMG_HEIGHT = 200;



		public Image(string path, out SurfComputationType usedType, string name = null) {

			path.ThrowIfNullOrEmpty();
			if (!File.Exists(path)) {
				throw new ArgumentException("File does not exist!");
			}

			imgPath = path;
			this.Name = name ?? Path.GetFileName(path);
			//cache file
			string surfFile = path.TrimEnd('/', '\\') + ".SURF";
			
			bool loaded = false;

			Program.Stopwatches[STOPWATCH_KEY].Start();

			//load from cache
			if (File.Exists(surfFile)) {
				try {
					Descriptors = loadFromFile(surfFile);
					loaded = true;
				} catch {
					loaded = false;
				}
			}

			if (loaded) {
				usedType = SurfComputationType.Cache;
				return;
			}

			using (EmguImg img = new EmguImg(path)) {

				SURFDetector sdCpu = new SURFDetector(HESSIAN_THRESH, false);
				usedType = SurfComputationType.GPU;

				/*
				if (img.Width > MIN_GPU_IMG_WIDTH && img.Height > MIN_GPU_IMG_HEIGHT && GpuInvoke.HasCuda) {
					loaded = true;
					try {
						GpuSURFDetector sdGpu = new GpuSURFDetector(sdCpu.SURFParams, 0.01f);
						var imgGpu = new EmguImgGpu(img);
						var imgGrey = imgGpu.Convert<Gray, Byte>();
						var keypoints = sdGpu.DetectKeyPointsRaw(imgGrey, null);
						Descriptors = sdGpu.ComputeDescriptorsRaw(imgGrey, null, keypoints).ToMatrix();
					} catch {
						loaded = false;
					}
				}
				*/
 
				if (!loaded) {
					usedType = SurfComputationType.CPU;
					var imgGrey = img.Convert<Gray, Byte>();
					var keypoints = sdCpu.DetectKeyPointsRaw(imgGrey, null);
					Descriptors = sdCpu.ComputeDescriptorsRaw(imgGrey, null, keypoints);
				}
	
			}

			Program.Stopwatches[STOPWATCH_KEY].Stop();

			try {
				saveToFile(Descriptors, surfFile);
			} catch { }

		}



		private void saveToFile(Matrix<float> matrix, string fileName) {
			StringBuilder sb = new StringBuilder();
			(new XmlSerializer(typeof(Matrix<float>))).Serialize(new StringWriter(sb), matrix);
			XmlDocument xDoc = new XmlDocument();
			xDoc.LoadXml(sb.ToString());
			xDoc.Save(fileName);
		}

		private Matrix<float> loadFromFile(string fileName) {
			XmlDocument xDoc = new XmlDocument();
			xDoc.LoadXml(File.ReadAllText(fileName));
			return (Matrix<float>)
				(new XmlSerializer(typeof(Matrix<float>))).Deserialize(new XmlNodeReader(xDoc));
		}



		private EmguImg loadImg() {
			if (!File.Exists(imgPath)) {
				return null;
			}
			EmguImg img;
			try {
				img = new EmguImg(imgPath);
			} catch {
				return null;
			}
			return img;
		}

		public void ShowImage() {
			EmguImg img = loadImg();
			if (img != null) {
				CvInvoke.cvShowImage(Name, img.Ptr);
			}
		}

		public void ShowSurfImage() {
			EmguImg img = loadImg();
			if (img == null) {
				return;
			}
			SURFDetector sd = new SURFDetector(HESSIAN_THRESH, false);
			var imgGrey = img.Convert<Gray, Byte>();
			var keypoints = sd.DetectKeyPointsRaw(imgGrey, null);
			var imgKeypoints = Features2DToolbox.DrawKeypoints(img, keypoints, new Bgr(System.Drawing.Color.Blue),
				Features2DToolbox.KeypointDrawType.DRAW_RICH_KEYPOINTS);
			CvInvoke.cvShowImage(Name + " SURF", imgKeypoints.Ptr);
		}



		public override string ToString() {
			return Name ?? "";
		}

	}
}
