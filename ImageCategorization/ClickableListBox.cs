﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ImageCategorization {
	
	/// <summary>
	/// Listbox that raises event on item doubleclick.
	/// </summary>
	class ClickableListBox : ListBox {

		public class ItemClickedEventArgs : EventArgs {
			public int ItemIdx { get; private set; }
			public ItemClickedEventArgs(int itemIdx) {
				ItemIdx = itemIdx;
			}
		}

		public event EventHandler<ItemClickedEventArgs> ItemDoubleClicked;

		protected override void OnMouseDoubleClick(MouseEventArgs e) {
			int idx = this.IndexFromPoint(e.Location);
			if (idx != System.Windows.Forms.ListBox.NoMatches) {
				ItemDoubleClicked.Raise(this, new ItemClickedEventArgs(idx));
			}
		}



	}
}
