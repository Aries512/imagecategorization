﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace ImageCategorization {
	public partial class Form1 : Form {


		private List<ImageCategory> categories = new List<ImageCategory>();

		private Categorizer categorizer = new Categorizer();
		
		
		
		public Form1() {
			InitializeComponent();
			Logger.TextBoxes.Add(this.textBoxLog);
			categorizationSettingsControl1.Populate(new Categorizer.Params());
			textBoxLog.Tag = "DoNotHide";
			setButtons();
			listBoxImagesTrain.ItemDoubleClicked += new EventHandler<ClickableListBox.ItemClickedEventArgs>(listBoxImagesTrain_ItemClicked);
			listBoxImagesTest.ItemDoubleClicked += new EventHandler<ClickableListBox.ItemClickedEventArgs>(listBoxImagesTest_ItemClicked);
		}






		#region GUI



		private ImageCategory getSelectedCategory() {
			return comboBoxCategories.SelectedIndex < 0 ? null : categories[comboBoxCategories.SelectedIndex];
		}

		private Image getSelectedTrainingImage() {
			return listBoxImagesTrain.SelectedIndex < 0 ? null : getSelectedCategory().TrainingImages[listBoxImagesTrain.SelectedIndex];
		}

		private Image getSelectedTestingImage() {
			return listBoxImagesTest.SelectedIndex < 0 ? null : getSelectedCategory().TestingImages[listBoxImagesTest.SelectedIndex];
		}



		//enables or disables buttons accordingly to current state
		private void setButtons() {
			buttonRemoveCategory.Enabled = categories.Count > 0;
			buttonTrain.Enabled = categories.Count > 1;
			buttonTestSelectedImage.Enabled = categorizer.Trained && getSelectedTestingImage() != null;
			buttonTestSelectedCategory.Enabled = categorizer.Trained && getSelectedCategory() != null;
			buttonTestAll.Enabled = categorizer.Trained;
			buttonTestUnknownImage.Enabled = categorizer.Trained;
		}



		void listBoxImagesTrain_ItemClicked(object sender, ClickableListBox.ItemClickedEventArgs e) {
			getSelectedTrainingImage().ShowImage();
			getSelectedTrainingImage().ShowSurfImage();
		}

		void listBoxImagesTest_ItemClicked(object sender, ClickableListBox.ItemClickedEventArgs e) {
			getSelectedTestingImage().ShowImage();
			getSelectedTestingImage().ShowSurfImage();
		}

		private void listBoxImagesTest_SelectedIndexChanged(object sender, EventArgs e) {
			setButtons();
		}



		private void comboBoxCategories_SelectedIndexChanged(object sender, EventArgs e) {
			listBoxImagesTrain.Items.Clear();
			listBoxImagesTest.Items.Clear();
			if (comboBoxCategories.SelectedIndex >= 0) {
				ImageCategory cat = categories[comboBoxCategories.SelectedIndex];
				cat.TrainingImages.ForEach(img => listBoxImagesTrain.Items.Add(img.ToString()));
				cat.TestingImages.ForEach(img => listBoxImagesTest.Items.Add(img.ToString()));
			}
			setButtons();
		}



		//greys out GUI when working for a longer time
		private void disableGui() {
			this.Cursor = Cursors.WaitCursor;
			foreach (Control control in this.Controls) {
				if ((string)control.Tag != "DoNotHide") {
					control.Enabled = false;
				}
			}
		}

		//return back from greyed-out state
		private void enableGui() {
			foreach (Control control in this.Controls) {
				control.Enabled = true;
			}
			this.Cursor = Cursors.Default;
			//don't forget to disable things that shouldn't be enabled now
			setButtons();
		}



		#endregion GUI






		#region Categories



		private void buttonAddCategory_Click(object sender, EventArgs e) {
			string start = Properties.Settings.Default.LastCategoryPath.IsNullOrEmpty() ?
				Directory.GetCurrentDirectory() : Properties.Settings.Default.LastCategoryPath;
			FolderBrowserDialog fbd = new FolderBrowserDialog() {
				SelectedPath = start,
				Description = "Select folder containing images of the new category.",
				ShowNewFolderButton = false,
			};
			if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
				string directory = fbd.SelectedPath;
				Properties.Settings.Default.LastCategoryPath = directory;
				Properties.Settings.Default.Save();
				disableGui();
				addCategory(directory);
				enableGui();
			}
			setButtons();
		}



		private void buttonAddCategories_Click(object sender, EventArgs e) {
			string start = Properties.Settings.Default.LastCategoryPath.IsNullOrEmpty() ?
				Directory.GetCurrentDirectory() : Properties.Settings.Default.LastCategoryPath;
			FolderBrowserDialog fbd = new FolderBrowserDialog() {
				SelectedPath = start,
				Description = "Select folder containing categories (subfolders) you wish to add.",
				ShowNewFolderButton = false,
			};
			if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
				string directory = fbd.SelectedPath;
				Properties.Settings.Default.LastCategoryPath = directory;
				Properties.Settings.Default.Save();
				SelectCategoriesForm f = new SelectCategoriesForm();
				f.ShowDialog();
				disableGui();
				f.Categories.ForEach(c => addCategory(Path.Combine(directory, c)));
				enableGui();
			}
			setButtons();
		}



		private bool addCategory(string path) {
			if (categories.Any(c => c.Name == Path.GetFileName(path))) {
				return false;
			}
			ImageCategory cat;
			try {
				cat = new ImageCategory(path, (int)numericUpDownTrainingSetSize.Value);
			} catch {
				return false;
			}
			categories.Add(cat);
			comboBoxCategories.Items.Add(cat);
			comboBoxCategories.SelectedIndex = comboBoxCategories.Items.Count - 1;
			categorizer.Trained = false;
			return true;
		}



		private void buttonRemoveCategory_Click(object sender, EventArgs e) {
			ImageCategory cat = getSelectedCategory();
			categories.Remove(cat);
			comboBoxCategories.Items.Remove(cat);
			comboBoxCategories.SelectedIndex = categories.Count - 1;
			comboBoxCategories_SelectedIndexChanged(this, EventArgs.Empty);
			categorizer.Trained = false;
			setButtons();
		}



		#endregion Categories






		#region Training



		private void buttonTrain_Click(object sender, EventArgs e) {
			categorizer.@params = categorizationSettingsControl1.GetParams();
			disableGui();
			categorizer.Train(categories);
			enableGui();
			setButtons();
		}



		#endregion Training






		#region Testing



		private void startTestTimers() {
			Program.Stopwatches[Categorizer.TEST_NEIGHBOUR_STOPWATCH_KEY].Reset();
			Program.Stopwatches[Categorizer.TEST_CLASSIFICATION_STOPWATCH_KEY].Reset();
			Program.Stopwatches["Testing time"].Restart();
		}

		private void stopTestTimers() {
			Program.Stopwatches.Log(Categorizer.TEST_NEIGHBOUR_STOPWATCH_KEY);
			Program.Stopwatches.Log(Categorizer.TEST_CLASSIFICATION_STOPWATCH_KEY);
			Program.Stopwatches.Log("Testing time");
			Logger.NewLine();
		}



		private void buttonTestUnknownImage_Click(object sender, EventArgs e) {
			OpenFileDialog ofd = new OpenFileDialog() {
				InitialDirectory = Properties.Settings.Default.LastCategoryPath,
				Multiselect = false,
			};
			if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
				string file = ofd.FileName;
				Properties.Settings.Default.LastCategoryPath = Path.GetDirectoryName(file);
				Properties.Settings.Default.Save();
				Image img;
				try {
					Image.SurfComputationType surfType;
					img = new Image(file, out surfType);
				} catch {
					MessageBox.Show("Incorrect file!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					return;
				}
				Logger.Add("Testing unknown image " + Path.GetFileName(file) + "...");
				startTestTimers();
				disableGui();
				string resCat;
				Tester.TestSample(img, categorizer, out resCat);
				enableGui();
				Logger.Add("Result: " + resCat);
				stopTestTimers();
			}
		}



		private void buttonTestSelectedImage_Click(object sender, EventArgs e) {
			Image img = getSelectedTestingImage();
			if (img == null) {
				return;
			}
			string catName = getSelectedCategory().Name;
			Logger.Add("Testing image " + img.Name + " from category " + catName + "...");
			startTestTimers();
			string res;
			disableGui();
			bool success = Tester.TestSample(img, categorizer, out res, catName);
			enableGui();
			Logger.Add((success ? "Correct" : "Incorrect") + " result: " + res);
			stopTestTimers();
		}


		private void buttonTestSelectedCategory_Click(object sender, EventArgs e) {
			ImageCategory cat = getSelectedCategory();
			Logger.Add("Testing category " + cat.Name + " (" + cat.TestingImages.Count + " images)...");
			startTestTimers();
			List<string> res; double success;
			disableGui();
			Tester.TestCategory(cat, categorizer, out res, out success);
			enableGui();
			Logger.Add("Results:");
			for (int i = 0; i < res.Count; ++i) {
				Logger.Add(cat.TestingImages[i].Name + ": " + res[i]);
			}
			Logger.Add("Success ratio: " + success.ToString("0.###"));
			stopTestTimers();
		}


		private void buttonTestAll_Click(object sender, EventArgs e) {
			Logger.Add("Testing all " + categories.Count + " categories (" +
				categories.Select(cat => cat.TestingImages.Count).Sum()
				+ " images)...");
			startTestTimers();
			List<List<string>> res; double totalSuccess, avgSuccess;
			disableGui();
			Tester.TestCategories(categories, categorizer, out res, out totalSuccess, out avgSuccess);
			enableGui();
			Logger.Add("Results:");
			Logger.Add("Total success ratio: " + totalSuccess.ToString("0.###"));
			Logger.Add("Average success ratio over classes: " + avgSuccess.ToString("0.###"));
			stopTestTimers();
		}



		#endregion Testing






		#region StripMenu



		private void deleteCachedSURFsToolStripMenuItem_Click(object sender, EventArgs e) {
			string start = Properties.Settings.Default.LastCategoryPath.IsNullOrEmpty() ?
				Directory.GetCurrentDirectory() : Properties.Settings.Default.LastCategoryPath;
			FolderBrowserDialog fbd = new FolderBrowserDialog() {
				SelectedPath = start,
				Description = "Select folder which will be recursively cleaned of cache files."
			};
			if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
				string directory = fbd.SelectedPath;
				Properties.Settings.Default.LastCategoryPath = directory;
				Properties.Settings.Default.Save();
				Logger.Add("Clearing cache files...");
				try {
					disableGui();
					foreach (string file in Directory.GetFiles(directory, "*", SearchOption.AllDirectories)) {
						if (endsWithOneOf(file.ToLower(), "surf", "txt", "xml")) {
							File.Delete(file);
						}
					}
					enableGui();
				} catch { }
				Logger.Add("Done.");
				Logger.NewLine();
			}
		}

		private bool endsWithOneOf(string str, params string[] endings) {
			return endings.Any(e => str.EndsWith(e));
		}



		private void helpToolStripMenuItem_Click(object sender, EventArgs e) {
			System.Diagnostics.Process.Start("readme.txt");
		}



		#endregion StripMenu





	}
}
