﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ImageCategorization {
	public partial class SelectCategoriesForm : Form {
		public SelectCategoriesForm() {
			InitializeComponent();
		}

		public List<string> Categories { get; private set; }

		private void button1_Click(object sender, EventArgs e) {
			Categories = textBox1.Text.Split('\n', '\r')
				.Where(s => !s.IsNullOrEmpty()).Select(s => s.Trim()).ToList();
			this.Close();
		}

	}
}
