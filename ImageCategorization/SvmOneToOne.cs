﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.ML;
using Emgu.CV.Structure;

namespace ImageCategorization {
	
	/// <summary>
	/// One-to-one SVM model for classifying more than two classes.
	/// Creates standard 2-class model for each pair of classes.
	/// When classifying, all models vote and class with highest
	/// number of votes is selected.
	/// </summary>
	class SvmOneToOne : ISvm {



		private bool _trained = false;
		public bool Trained {
			get {
				return _trained;
			}
		}

		private int categoryCnt;

		//model for each pair of category
		//using only i<j for models[i,j]
		private SVM[,] models = null;





		public void Train(Matrix<float> data, Matrix<float> categories, Categorizer.Params @params) {

			if (data.Rows != categories.Rows) {
				throw new ArgumentException("Data and its categories must have same number of rows!");
			}

			//categories and number of their samples among data
			Dictionary<float, int> categorySet = new Dictionary<float, int>();
			for (int i = 0; i < categories.Rows; ++i) {
				if (categorySet.ContainsKey(categories[i, 0])) {
					categorySet[categories[i, 0]]++;
				} else {
					categorySet.Add(categories[i, 0], 1);
				}

			}
			categoryCnt = categorySet.Keys.Count;
			List<float> categoryList = new List<float>(categorySet.Keys);
			models = new SVM[categoryCnt, categoryCnt];

			//each pair
			for (int i = 0; i < categoryCnt; ++i) {
				for (int j = i + 1; j < categoryCnt; ++j) {

					models[i, j] = new SVM();
					float category1 = categoryList[i], category2 = categoryList[j];
					//data and categories of current pair
					Matrix<float> data12 = new Matrix<float>(
						categorySet[category1] + categorySet[category2], data.Cols);
					Matrix<float> categories12 = new Matrix<float>(data12.Rows, 1);
					int idx = 0;

					//find all the samples of current two categories
					for (int k = 0; k < data.Rows; ++k) {
						if (categories[k, 0].In(category1, category2)) {
							for (int m = 0; m < data.Cols; ++m) {
								data12[idx, m] = data[k, m];
							}
							categories12[idx++, 0] = categories[k, 0] == category1 ? category1 : category2;
						}
					}

					SVMParams p = new SVMParams() {
						KernelType = @params.SvmKernelType == Categorizer.Params.SVMKernelType.RBF ?
							Emgu.CV.ML.MlEnum.SVM_KERNEL_TYPE.RBF : Emgu.CV.ML.MlEnum.SVM_KERNEL_TYPE.LINEAR,
						SVMType = Emgu.CV.ML.MlEnum.SVM_TYPE.C_SVC,
						Gamma = @params.SvmGamma,
						C = 1,
						TermCrit = new MCvTermCriteria(100, 0.00001),
					};
					models[i, j].TrainAuto(data12, categories12, null, null, p.MCvSVMParams, @params.SvmKfold);
				}
			}

			_trained = true;

		}





		public float Classify(Emgu.CV.Matrix<float> datum) {

			if (!_trained) {
				throw new InvalidOperationException("Must be trained first!");
			}

			Dictionary<float, int> votes = new Dictionary<float, int>();

			//each pair votes
			for (int i = 0; i < categoryCnt; ++i) {
				for (int j = i + 1; j < categoryCnt; ++j) {
					float vote = models[i, j].Predict(datum);
					if (votes.ContainsKey(vote)) {
						votes[vote]++;
					} else {
						votes.Add(vote, 1);
					}
				}
			}

			//find category with highest number of votes
			float best = 0;
			int bestCnt = 0;
			foreach (var rec in votes) {
				if (rec.Value > bestCnt) {
					bestCnt = rec.Value;
					best = rec.Key;
				}
			}
			return best;

		}


	}
}
