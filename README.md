# Automatic content-based image categorization

Implementation of CBIR image categorizer using EmguCV (OpenCV C# port) based on my [paper](https://bitbucket.org/Aries512/imagecategorization/src/3f84a44dfa8b53181e48d538705c20be57236e53/paper/paper.pdf?at=master) (slovak only). Extracts SURF features to form visual words histogram which serves as an input for SVM-based classifier. Process of feature extraction and classifier training can be customized by selecting appropriate parameters for each part of the algorithm.

### How to run
1. Compile (duh).
2. Copy *x86* folder containing OpenCV libraries to your *Release* or *Debug* folder.
3. Run (duh again).

### How to use
1. First, add some image categories. A category is represented by a simple folder containing some image files. By clicking on "Add category", you can select single folder. Alternatively, by clicking on "Add categories", you can select parent folder which contains multiple subfolders - your desired categories. After selecting parent folder, you can type category (folder) names, each on new line, which will be then added.
After adding new category, SURF features of images will be created and stored on disk. Be aware that feature extraction may take some time.
Training set size determines how many images will be used for classifier training.

2. After adding at least two categories, you can train classifier. By adding or removing any category, classifier becomes outdated and has to be trained again. You can customize multiple parameters of the classifier.

3. After successful training, you can either:
      + Classify unknown images.
      + Test classification on any image from the testing set.
      + Test classification on the whole category (its testing images).
      + Test all testing images of all categories.
   Testing is possible only with trained, up-to-date classifier.
   
4. Application creates cache files to store extracted SURF descriptors for each image. You can erase all these files in the selected directory by using File -> Delete cached SURFs.

5. Beware - application can take up a lot of space, sometimes more than 1GB. Adding too many categories and training classifier with extreme parameters may cause application to crash.